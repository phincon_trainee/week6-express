import express from "express";
import fs from "fs/promises";
import { v4 as uuidv4 } from "uuid";

const app = express();
const port = 3000;

app.use(express.json());

const dbFilePath = "./Database/db.json";

app.get("/cats", async (req, res) => {
  try {
    const data = await fs.readFile(dbFilePath, "utf-8");
    const dataParse = JSON.parse(data);
    res.status(200).json(dataParse.cats);
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: "Internal Server Error" });
  }
});

app.get("/cats/:id", async (req, res) => {
  try {
    const data = await fs.readFile(dbFilePath, "utf-8");
    const dataParse = JSON.parse(data);
    const ras = dataParse.cats;

    const item = ras.find((item) => item.id == req.params.id);

    if (item) {
      res.status(200).json(item);
    } else {
      res.status(404).json({ message: "Not Found" });
    }
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: "Internal Server Error" });
  }
});

app.post("/cats", async (req, res) => {
  try {
    const data = await fs.readFile(dbFilePath, "utf-8");
    const dataParse = JSON.parse(data);

    const newCat = {
      id: uuidv4(),
      ras: req.body.ras,
      food: req.body.food,
    };

    dataParse.cats.push(newCat);

    await fs.writeFile(dbFilePath, JSON.stringify(dataParse, null, 2));

    res.sendStatus(201);
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: "Internal Server Error" });
  }
});

app.put("/cats/:id", async (req, res) => {
  try {
    const data = await fs.readFile(dbFilePath, "utf-8");
    const dataParse = JSON.parse(data);
    const ras = dataParse.cats;

    const item = ras.find((item) => item.id == req.params.id);

    if (item) {
      item.ras = req.body.ras;
      item.food = req.body.food;

      await fs.writeFile(dbFilePath, JSON.stringify(dataParse, null, 2));

      res.status(200).json(item);
    } else {
      res.status(404).json({ message: "Not Found" });
    }
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: "Internal Server Error" });
  }
});

app.delete("/cats/:id", async (req, res) => {
  try {
    const data = await fs.readFile(dbFilePath, "utf-8");
    const dataParse = JSON.parse(data);
    const ras = dataParse.cats;

    const itemIndex = ras.findIndex((item) => item.id == req.params.id);

    if (itemIndex !== -1) {
      ras.splice(itemIndex, 1);

      await fs.writeFile(dbFilePath, JSON.stringify(dataParse, null, 2));

      res.status(200).json({ message: "Delete success" });
    } else {
      res.status(404).json({ message: "Not Found" });
    }
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: "Internal Server Error" });
  }
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
