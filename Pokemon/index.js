import express from "express";
import axios from "axios";
import fs from "fs";
import { v4 } from "uuid";
const app = express();
const port = 3001;

app.use(express.json());
// module.exports = app;  //non activkan jika ingin menjalankan unit testing
const dbFilePathJSon = "./db.json";
const dbFilePath = "https://pokeapi.co/api/v2/pokemon?limit=200";
const initialData = {
  capturedPokemon: [],
};

export const isPrime = (num) => {
  if (num <= 1) return false;
  if (num <= 3) return true;

  if (num % 2 === 0 || num % 3 === 0) return false;

  for (let i = 5; i * i <= num; i += 6) {
    if (num % i === 0 || num % (i + 2) === 0) return false;
  }
  return true;
};

export const fibonacci = (index) => {
  if (index === 0) return 0;
  if (index === 1) return 1;

  let prev = 0;
  let current = 1;

  for (let i = 2; i <= index; i++) {
    const next = prev + current;
    prev = current;
    current = next;
  }

  return current;
};

// Middleware to check if a number is prime before releasing a Pokemon
const checkRelease = (req, res, next) => {
  const randomNumber = Math.floor(Math.random() * 100);
  if (isPrime(randomNumber)) {
    res.locals.randomNumber = randomNumber;
    next();
  } else {
    res.status(201).json({
      successRelease: false,
      randomNumber: randomNumber,
      message: `You got ${randomNumber}! Failed to release pokemon`,
    });
  }
};

// Get Pokemon List
app.get("/Pokemon", async (req, res) => {
  try {
    const response = await axios.get(dbFilePath);
    const pokemonList = response.data.results.map((pokemon) => pokemon.name);
    // const pokemonList = response.data.results;
    res
      .status(200)
      .json({ status: "success", statusCode: 200, name: pokemonList });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Internal Server Error" });
  }
});

// Get Pokemon Detail
app.get("/pokemon/:name", async (req, res) => {
  const { name } = req.params;
  try {
    const response = await axios.get(
      `https://pokeapi.co/api/v2/pokemon/${name}`
    );
    const pokemonDetail = response.data;
    res.json(pokemonDetail);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Internal Server Error" });
  }
});

// Catch pokemon
app.post("/pokemon/catch", async (req, res) => {
  try {
    const { pokemonName } = req.body;
    // console.log(pokemonName);
    const successProbability = Math.random() < 0.5;

    if (successProbability) {
      const data = fs.readFileSync(dbFilePathJSon, "utf-8");
      const dataParse = JSON.parse(data);

      // Jika dataParse tidak memiliki capturedPokemon, inisialisasi dengan array kosong
      if (!dataParse.capturedPokemon) {
        dataParse.capturedPokemon = [];
      }

      dataParse.capturedPokemon.push({
        id: v4(),
        name: pokemonName,
        release: 0,
      });

      fs.writeFileSync(dbFilePathJSon, JSON.stringify(dataParse, null, 2)); // Menyimpan perubahan kembali ke file

      res.json({
        status: "success",
        statusCode: 200,
        message: "success catch pokemon",
      });
    } else {
      res.json({
        status: "failed",
        statusCode: 404,
        message: "failed catch pokemon",
      });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal Server Error" });
  }
});

// get mypokemon list (successful captured pokemon)
app.get("/mypokemon", async (req, res) => {
  try {
    const data = await fs.promises.readFile(dbFilePathJSon, "utf-8");
    const dataParse = JSON.parse(data);
    const capturedPokemonList = dataParse.capturedPokemon || [];

    // res.status(200).json(capturedPokemonList);
    res.json({ status: "Success", statusCode: 200, data: capturedPokemonList });
  } catch (err) {
    console.log(err);
    res.status(500).json({ message: "Internal Server Error" });
  }
});

// release pokemon
app.post("/pokemon/release", checkRelease, async (req, res) => {
  try {
    const { randomNumber } = res.locals;
    const data = await fs.readFileSync(dbFilePathJSon, "utf-8");
    const dataParse = JSON.parse(data);
    const capturedPokemonList = dataParse.capturedPokemon || [];

    if (capturedPokemonList.length === 0) {
      res.json({ message: "no capturedPokemon" });
    } else {
      const releasePokemon = capturedPokemonList.pop();
      await fs.writeFile(
        dbFilePathJSon,
        JSON.stringify(dataParse, null, 2),
        (err) => {
          if (err) {
            console.log(err);
            res.status(500).json({
              status: "failed",
              message: "Failed to release pokemon",
            });
          } else {
            res.json({
              status: "success",
              statusCode: 200,
              message: "successfully release pokemon",
              releasePokemon: releasePokemon,
              randomNumber: randomNumber,
            });
          }
        }
      );
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Internal Server Error" });
  }
});

// rename pokemon
app.put("/pokemon/rename/:id", async (req, res) => {
  try {
    const { id } = req.params.id;
    const data = await fs.promises.readFile(dbFilePathJSon, "utf-8");
    const dataParse = JSON.parse(data);
    const dataPokemon = dataParse.capturedPokemon.find((p) => {
      if (p.id === req.params.id) {
        return p;
      }
    });

    const splitName = dataPokemon.name.split("-");
    const originalName = splitName[0];
    const release = dataPokemon.release;
    const fiboKey = fibonacci(release);
    const newName = `${originalName}-${fiboKey}`;
    dataPokemon.release = release + 1;
    dataPokemon.name = newName;

    await fs.promises.writeFile(
      dbFilePathJSon,
      JSON.stringify(dataParse, null, 2),
      "utf-8"
    );

    res.json({
      status: "success",
      statusCode: 200,
      message: "successfully rename pokemon",
      newName: newName,
      release: release,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Internal Server Error" });
  }
});

// Handle not found routes
app.use((req, res, next) => {
  res.status(404).json({ message: "Not Found" });
});

// Handle errors
app.use((err, req, res, next) => {
  console.log(err);
  res.status(500).json({ message: "Internal Server Error" });
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
