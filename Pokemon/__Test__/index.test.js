import request from "supertest";
import { get } from "axios";
import app from "../index";
import { promises } from "fs";

jest.mock("axios");

export default app; // Export the Express app

describe("GET /Pokemon", () => {
  it("should return a list of Pokémon names", async () => {
    const mockResponse = {
      data: {
        results: [{ name: "Pikachu" }, { name: "Charmander" }],
      },
    };

    get.mockResolvedValue(mockResponse);

    const response = await request(app).get("/Pokemon");

    expect(response.status).toBe(200);
    expect(response.body).toEqual({
      status: "success",
      statusCode: 200,
      name: ["Pikachu", "Charmander"], // Adjust with the actual names
    });
  });

  it("should handle internal server error", async () => {
    get.mockRejectedValue(new Error("Failed to fetch data"));

    const response = await request(app).get("/Pokemon");

    expect(response.status).toBe(500);
    expect(response.body).toEqual({
      message: "Internal Server Error",
    });
  });
});

describe("GET /pokemon/:name", () => {
  it("should return the details of a specific pokemon", async () => {
    const mockName = "metapod";
    const mockResponse = {
      data: {
        name: mockName,
        type: "Electric",
      },
    };

    get.mockResolvedValue(mockResponse);

    const response = await request(app).get(`/pokemon/${mockName}`);

    expect(response.status).toBe(200);
    expect(response.body).toEqual({
      // status: "success",
      // statusCode: 200,
      name: mockName,
      type: "Electric",
    });
  });

  it("should handle errors and return 500 status on internal error", async () => {
    const mockName = "nonexistentpokemon"; // Mock a non-existent Pokémon name

    // Mock axios.get to throw an error
    get.mockRejectedValue(new Error("An error occurred"));

    const response = await request(app).get(`/pokemon/${mockName}`);

    expect(response.status).toBe(500);
    expect(response.body).toEqual({ message: "Internal Server Error" });
  });
});

describe("GET /mypokemon", () => {
  it("should return a list of captured Pokémon", async () => {
    // Mock the data to be read from the file
    const mockData = {
      capturedPokemon: [
        { id: "1", name: "Pikachu", release: 0 },
        { id: "2", name: "Charmander", release: 1 },
      ],
    };
    jest
      .spyOn(promises, "readFile")
      .mockResolvedValue(JSON.stringify(mockData));

    const response = await request(app).get("/mypokemon");

    expect(response.status).toBe(200);
    expect(response.body).toEqual({
      status: "Success",
      statusCode: 200,
      data: mockData.capturedPokemon,
    });
  });

  it("should handle errors and return 500 status on internal error", async () => {
    // Mock fs.promises.readFile to throw an error
    jest
      .spyOn(promises, "readFile")
      .mockRejectedValue(new Error("An error occurred"));

    const response = await request(app).get("/mypokemon");

    expect(response.status).toBe(500);
    expect(response.body).toEqual({
      message: "Internal Server Error",
    });
  });
});

describe("POST /pokemon/release", () => {
  it("should release a captured Pokémon successfully", async () => {
    // Mock axios.get to return a prime random number
    get.mockResolvedValue({
      data: { successRelease: true, randomNumber: 7 },
    });

    const response = await request(app).post("/pokemon/release");

    expect(response.status).toBe(200);
    expect(response.body).toEqual({
      status: "success",
      statusCode: 200,
      message: "successfully release pokemon",
      successRelease: true,
      randomNumber: 7,
    });
  });

  it("should handle errors and return 201 status on unsuccessful release", async () => {
    // Mock axios.get to return a non-prime random number
    get.mockResolvedValue({
      data: { successRelease: false, randomNumber: 9 },
    });

    const response = await request(app).post("/pokemon/release");

    expect(response.status).toBe(201); // Update the expected status code to 201
    expect(response.body).toEqual({
      status: "failed",
      statusCode: 201, // Update the expected status code to 201
      message: "You got 9! Failed to release pokemon", // Update the failure message
      successRelease: false,
      randomNumber: 9,
    });
  });
});

describe("PUT /pokemon/rename/:id", () => {
  it("should rename a captured Pokémon successfully", async () => {
    // Mock the data to be read from the file
    const mockData = {
      capturedPokemon: [
        { id: "1", name: "Pikachu-0", release: 0 },
        { id: "2", name: "Charmander-1", release: 1 },
      ],
    };
    jest
      .spyOn(promises, "readFile")
      .mockResolvedValue(JSON.stringify(mockData));
    jest.spyOn(promises, "writeFile").mockResolvedValue();

    const response = await request(app).put("/pokemon/rename/1");

    expect(response.status).toBe(200);
    expect(response.body).toEqual({
      status: "success",
      statusCode: 200,
      message: "successfully rename pokemon",
      newName: expect.any(String), // Assuming you want to test the newName field
      release: expect.any(Number), // Assuming you want to test the release field
    });
  });

  it("should handle errors and return 500 status on internal error", async () => {
    // Mock fs.promises.readFile to throw an error
    jest
      .spyOn(promises, "readFile")
      .mockRejectedValue(new Error("An error occurred"));

    const response = await request(app).put("/pokemon/rename/1");

    expect(response.status).toBe(500);
    expect(response.body).toEqual({
      message: "Internal Server Error",
    });
  });

  it("should handle not found Pokémon and return 404 status", async () => {
    // Mock fs.promises.readFile to read an empty list of captured Pokémon
    jest
      .spyOn(promises, "readFile")
      .mockResolvedValue(JSON.stringify({ capturedPokemon: [] }));

    const response = await request(app).put("/pokemon/rename/1");

    expect(response.status).toBe(500);
    expect(response.body).toEqual({
      message: "Internal Server Error",
    });
  });
});

describe("Error handling middleware", () => {
  it("should return 500 status and error message", async () => {
    const response = await request(app).get("/nonexistent-route");

    expect(response.status).toBe(404);
    expect(response.body).toEqual({
      message: "Not Found",
    });
  });
});
